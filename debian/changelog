bitshuffle (0.3.5-4.1) UNRELEASED; urgency=medium

  * added provides and autopkgtests after
    https://salsa.debian.org/science-team/h5f-packaging-guidelines
    Closes: #1032583

 -- Enrico Zini <enrico@debian.org>  Thu, 09 Mar 2023 14:57:19 +0100

bitshuffle (0.3.5-4) unstable; urgency=medium

  [Picca Frédéric-Emmanuel]
  * Bug fix: "Unvendor liblzf", thanks to Timo Röhling (Closes: #958322).
  * Unvendor liblz4
  * Built hdf5 plugins for serial and openmpi version of hdf5, and install
    them at the expected default location.

  [Thorsten Alteholz]
  * debian/control: bump standard to 3.6.0
  * debian/control: set Rules-Requires-Root: no

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 07 Feb 2022 22:03:38 +0100

bitshuffle (0.3.5-3.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Drew Parsons <dparsons@debian.org>, Gilles Filippini <pini@debian.org> ]
  * Closes: #955456
    - fix-deprecated.patch: fix test_h5filter.py and test_h5plugin.py
      to open files with flag 'w' when required
    - Build-Depends: python3-h5py-mpi to force using the mpi flavour
      of h5py
    - override_dh_auto_test:
      - Run the tests via mpirun so that h5py knows it has to invoke its
        mpi implementation
      - Launch the tests for each python version separately to permit MPI
        initialization at each run

 -- Gilles Filippini <pini@debian.org>  Tue, 07 Apr 2020 20:50:47 +0200

bitshuffle (0.3.5-3) unstable; urgency=medium

  * don't use -march=native when building the package

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 01 Dec 2019 19:03:38 +0100

bitshuffle (0.3.5-2) unstable; urgency=medium

  * the build failure reported on s390x was triggered by an issue
    with openmpi (which also introduced the -march=z15 flag)
    (Closes: #945331)
  * debian/control: bump standard to 4.4.1 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 30 Nov 2019 19:03:38 +0100

bitshuffle (0.3.5-1) unstable; urgency=medium

  * Initial release

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 20 Aug 2019 19:29:38 +0200
